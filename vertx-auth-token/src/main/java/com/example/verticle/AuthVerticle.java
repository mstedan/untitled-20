package com.example.verticle;

import com.example.common.annotation.DeployVerticle;
import com.example.common.handler.RouteHandler;
import com.example.common.jwt.AdminAuthenticationHandler;
import com.example.common.jwt.provider.AuthTokenProvider;
import com.nimbusds.jose.jwk.ECKey;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.AuthenticationHandler;
import io.vertx.ext.web.handler.BodyHandler;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Slf4j
@DeployVerticle(2)
@Component
@Scope(SCOPE_PROTOTYPE)/*由于将部署此垂直领域的多个实例，因此需要原型范围*/
public class AuthVerticle extends AbstractVerticle {

    static final int PORT = 3000;

    static final String HOST = "127.0.0.1";

    static final AtomicInteger counter = new AtomicInteger();

    @Resource
    private ECKey secret;

    @Resource
    AuthTokenProvider authProvider;

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        RouteHandler routeHandler = new RouteHandler();
        AuthenticationHandler authHandler = new AdminAuthenticationHandler(authProvider);

        router.route().failureHandler(routeHandler::handleError);
        router.route("/api/*")
                .order(-10)
                .handler(authHandler);


        // 执行请求操作
        router.route("/api/hello").method(HttpMethod.GET).handler(context -> {
            context.json(new JsonObject("{\"status\":200,\"msg\":\"hello\"}"));
        });

        router.route("/api/data").method(HttpMethod.GET).handler(context -> {
            JsonObject result = new JsonObject();
            result.put("msg", "请求成功");
            result.put("data", System.currentTimeMillis());
            context.response().setStatusCode(200).end(Json.encode(result));
        });

        router.route("/getToken").method(HttpMethod.GET).handler(this::getToken);

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(PORT, HOST)
                .onSuccess(http -> {
                    log.info("HttpServer-{} is runnig on localhost:{}", counter.incrementAndGet(), http.actualPort());
                    startPromise.complete();
                }).onFailure(t -> {
                    log.error("HttpServer running failed");
                    startPromise.fail(t);
                });

    }

    private void getToken(RoutingContext context) {
        String username = context.request().getParam("username");
        String password = context.request().getParam("password");
        if (Objects.equals(username, "zhangsan") && Objects.equals(password, "123456")) {
            List<String> permissions = new ArrayList<>();
            permissions.add("/api/hello");
            permissions.add("/api/data");
            //setting JWT claims
            JsonObject claims = new JsonObject();
            claims.put("role", "admin");
            claims.put("permissions", permissions);
            claims.put("sub", username);

            String token = authProvider.generateToken(claims);
            context.response().setStatusCode(200)
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .end(token);
        }
    }
}
