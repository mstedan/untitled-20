package com.example.common.config;


import com.example.common.jwt.provider.AuthTokenProvider;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.web.Router;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class VertxConfiguration {

    @Bean
    public Vertx vertx() {
        return Vertx.vertx();
    }

    @Bean
    public Router router(Vertx vertx){
        return Router.router(vertx);
    }

    @Bean
    public EventBus eventBus(Vertx vertx) {
        return vertx.eventBus();
    }

    @Bean
    public ECKey ECKeyGenerator() throws JOSEException {
        return new ECKeyGenerator(Curve.P_256).keyID(UUID.randomUUID().toString()).generate();
    }

    @Bean
    public AuthTokenProvider authTokenProvider(ECKey secret){
        ECKey key = secret.toPublicJWK();
        return new AuthTokenProvider(key, secret);
    }
}
