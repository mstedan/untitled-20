package com.example.common.jwt.provider;

import com.example.common.constant.CommonConstants;
import com.example.common.jwt.AuthUser;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.JWTOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class AuthTokenProvider implements JWTAuth {
    private final ECKey key;
    private final ECKey secret;

    public AuthTokenProvider(ECKey key, ECKey secret) {
        this.key = key;
        this.secret = secret;
    }

    @Override
    public String generateToken(JsonObject data, JWTOptions jwtOptions) {
        return null;
    }

    @Override
    public String generateToken(JsonObject data) {
        try {
            JWSSigner signer = new ECDSASigner(secret);
            //get payload
            String subject = data.getString(CommonConstants.SUB);
            String role = data.getString(CommonConstants.ROLE);
            JsonArray permissions = data.getJsonArray(CommonConstants.PERMISSIONS);
            //设置到期时间->token有效期为30天
            LocalDateTime expired = LocalDateTime.now().plusDays(30);
            //将“expired”转换为Date，因为JWTClaimsSet接受Date作为参数
            Date date = Date.from(expired.atZone(ZoneId.systemDefault()).toInstant());
            //创建JWT claims
            JWTClaimsSet claims = new JWTClaimsSet.Builder()
                    .subject(subject)
                    .expirationTime(date)
                    .claim(CommonConstants.ROLE, role)
                    .claim(CommonConstants.PERMISSIONS, permissions)
                    .build();

            //创建令牌和签名
            SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(secret.getKeyID()).build(), claims);
            signedJWT.sign(signer);

            //返回创建令牌
            return signedJWT.serialize();
        } catch (Exception ex) {
            //Log error to STDOUT
            System.out.println("Unable to generate token: " + ex.getLocalizedMessage());
            //return null result
            return null;
        }
    }

    @Override
    public void authenticate(JsonObject credentials, Handler<AsyncResult<User>> handler) {
        //获取JWT令牌
        String token = credentials.getString("token");
        //检查令牌是否有效
        try {
            //验证令牌
            JWSVerifier verifier = new ECDSAVerifier(key);
            SignedJWT signedJWT = SignedJWT.parse(token);
            if (!signedJWT.verify(verifier)) {
                //我们无法验证此令牌->抛出异常
                handler.handle(Future.failedFuture("该令牌无效"));
            }
            //从令牌获取用户名
            String name = signedJWT.getJWTClaimsSet().getSubject();
            //从令牌获取用户角色
            String role = signedJWT.getJWTClaimsSet().getStringClaim("role");
            Set<String> permissions = new HashSet<>(signedJWT.getJWTClaimsSet().getStringListClaim("permissions"));
            //List<String> permissions = signedJWT.getJWTClaimsSet().getStringListClaim("permissions");
            //创建具有名称和指定权限的AuthUser对象
            AuthUser user = new AuthUser(name, role, permissions);
            //用户已成功进行身份验证
            handler.handle(Future.succeededFuture(user));
        } catch (Exception ex) {
            //user is not authorized
            handler.handle(Future.failedFuture(ex));
        }
    }
}
