package com.example.common.jwt;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authorization.Authorization;
import io.vertx.ext.auth.impl.UserImpl;

import java.util.*;

public class AuthUser extends UserImpl {
    private String name;
    private String role;
    Set<String> permissions = new HashSet<>();

    public AuthUser(String name, String role, Set<String> permissions) {
        this.name = name;
        this.role = role;
        this.permissions = permissions;
    }

    public boolean checkPermission(String request, String cached) {
        return cached.equals("*") || request.equals(cached) || request.contains(".html") || request.contains(".css") || request.contains(".json") || request.contains(".text") || request.contains(".js") || request.contains(".woff") || request.contains(".svg") || request.contains(".ttf") || request.contains(".ico") || request.contains(".png");
    }
    protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        boolean access = this.permissions.stream().anyMatch((per) -> {
            return this.checkPermission(permission, per);
        });
        resultHandler.handle(Future.succeededFuture(access));
    }
    @Override
    public User isAuthorized(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        if (this.permissions.contains(permission)) {
            resultHandler.handle(Future.succeededFuture(true));
        } else {
            this.doIsPermitted(permission, (res) -> {
                if (res.succeeded() && (Boolean)res.result()) {
                    this.permissions.add(permission);
                }
                resultHandler.handle(res);
            });
        }
        return this;
    }

    public void addPermissions(Collection<String> permissions) {
        this.permissions.addAll(permissions);
    }
}
