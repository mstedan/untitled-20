package com.example.common.jwt;

import com.example.common.constant.CommonConstants;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authentication.AuthenticationProvider;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.HttpException;
import io.vertx.ext.web.handler.impl.HTTPAuthorizationHandler;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminAuthenticationHandler extends HTTPAuthorizationHandler<AuthenticationProvider> {

    @Resource
    private Vertx vertx;
    private final Logger logger = LoggerFactory.getLogger(AdminAuthenticationHandler.class);
    static final Type TOKEN_TYPE = Type.BEARER;

    public AdminAuthenticationHandler(AuthenticationProvider authProvider) {
        super(authProvider, TOKEN_TYPE, null);
    }
//    public AdminAuthenticationHandler(AuthenticationProvider authProvider, String realm) {
//        super(authProvider, TOKEN_TYPE, realm);
//    }

    @Override
    public void authenticate(RoutingContext context, Handler<AsyncResult<User>> handler) {
        parseAuthorization(context, parseAuthorization -> {

            if (parseAuthorization.failed()) {
                handler.handle(Future.failedFuture(parseAuthorization.cause()));
                return;
            }

            JsonObject credentials = new JsonObject().put(CommonConstants.TOKEN, parseAuthorization.result());
            authProvider.authenticate(credentials, authn -> {
                if (authn.failed()) {
                    handler.handle(Future.failedFuture(new HttpException(401, authn.cause())));
                } else {
                    String permission = context.request().path();
                    AuthUser user = (AuthUser) authn.result();
                    user.doIsPermitted(permission, res -> {
                        if (res.succeeded() && res.result()) {
                            logger.info("拦截请求：" + permission + ", 允许匿名：否， 当前token：" + parseAuthorization.result() + ", 校验结果：允许访问");
                            context.next();
                        } else {
                            logger.info("拦截请求：" + permission + ", 允许匿名：否， 当前token：" + parseAuthorization.result() + ", 校验结果：用户权限不足，不允许访问");
                            context.response().setStatusCode(403).end("you have no permission to access '" + context.request().path() + "'");
                        }
                    });
                }
            });
        });
    }
}
