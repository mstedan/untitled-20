package com.example.common.handler;

import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.HttpException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RouteHandler {
    public void handleError(RoutingContext rtx) {

        ErrorModel errorModel = null;

        Throwable t = rtx.failure();

        log.warn("HTTP {} {} 处理异常", rtx.request().method(), rtx.request().path(), t);

        if (t instanceof HttpException) {
            errorModel = ErrorModel.of(((HttpException) t).getStatusCode(),
                    ((HttpException) t).getPayload());
        }

        errorModel = ErrorModel.of(rtx.statusCode(), rtx.failure().getMessage());

        rtx.response().setStatusCode(errorModel.code)
                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .end(errorModel.toJson().toBuffer());

    }
}
