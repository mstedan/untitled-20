package com.example.common.listener;


import com.example.common.annotation.DeployVerticle;
import com.example.common.spring.context.SpringContextHolder;
import com.example.common.spring.factory.SpringVerticleFactory;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.spi.VerticleFactory;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class DeployVertxListener implements ApplicationListener<ApplicationReadyEvent> {

    @Resource
    private Vertx vertx;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ConfigurableApplicationContext applicationContext = event.getApplicationContext();
        SpringContextHolder.setContext(applicationContext);

        VerticleFactory verticleFactory = SpringContextHolder.getBean(SpringVerticleFactory.class);
        vertx.registerVerticleFactory(verticleFactory);

        SpringContextHolder.getApplicationContext().getBeansWithAnnotation(DeployVerticle.class).values().forEach(verticle -> {
            int value = verticle.getClass().getDeclaredAnnotation(DeployVerticle.class).value();
            DeploymentOptions deploymentOptions = new DeploymentOptions().setInstances(value);
            vertx.deployVerticle(verticleFactory.prefix() + ":" + verticle.getClass().getName(), deploymentOptions, r -> {
                if (r.succeeded()) {
                    log.info("==========================={}服务已启动=========================", verticle.getClass().getSimpleName());
                } else {
                    log.error(r.cause().toString());
                }
            });
        });
    }
}
