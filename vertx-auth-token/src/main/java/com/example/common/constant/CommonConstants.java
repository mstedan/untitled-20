package com.example.common.constant;

public interface CommonConstants {
    String TOKEN = "token";
    String SUB = "sub";
    String ROLE = "role";
    String PERMISSIONS = "permissions";
}
