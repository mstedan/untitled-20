package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Auth3Application {
    public static void main(String[] args) {
        SpringApplication.run(Auth3Application.class, args);
    }
}
