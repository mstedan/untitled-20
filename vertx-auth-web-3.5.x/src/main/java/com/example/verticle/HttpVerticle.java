package com.example.verticle;


import com.example.auth.MyAuthProviderImpl;
import com.example.auth.MyPermissionStrategyImpl;
import com.example.auth.MySessionIdStrategyImpl;
import com.example.common.annotation.DeployVerticle;
import com.example.common.base.SubRouter;
import com.example.common.config.HttpProperties;
import com.example.common.handler.CrosHandler;
import com.example.common.handler.RequestParamsHandler;
import com.example.common.handler.ResponsePostHandler;
import com.example.common.handler.ResponsePreHandler;
import com.example.common.toolkit.spring.context.SpringContextHolder;
import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import fun.bookish.vertx.auth.simple.handler.SimpleAuthHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.StaticHandler;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

/**
 * http服务部署单元
 */
@Slf4j
@DeployVerticle
@Component
@Scope(SCOPE_PROTOTYPE)/*由于将部署此垂直领域的多个实例，因此需要原型范围*/
public class HttpVerticle extends AbstractVerticle {
    private final HttpProperties httpProperties = SpringContextHolder.getBean(HttpProperties.class);

    @Override
    public void start() throws Exception {

        Router router = Router.router(vertx);
        // 挂载所有前置处理器
//        router.route()
//                .handler(new CrosHandler())                     // 跨域处理器
//                .handler(CookieHandler.create())                // cookie处理器
//                .handler(authHandler)                           // 权限认证处理器
//                .handler(BodyHandler.create())                  // 请求体处理器
//                .handler(new RequestParamsHandler())            // 请求参数处理器
//                .handler(new ResponsePreHandler())              // 响应前置处理器(设置字符编码等)
//                .handler(staticHandler);                        // 静态资源处理器


        configRouter(router);        //配置路由

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(httpProperties.getPort(), res -> {
                    if(res.succeeded()){
                        log.info("http 服务启动成功, port = " +  httpProperties.getPort());
                    }else{
                        log.error("http 服务启动失败", res.cause());
                        throw new IllegalStateException(res.cause());
                    }
                });

    }

    private void configRouter(Router router) {
        // 静态资源处理器
        StaticHandler staticHandler = StaticHandler.create().setIndexPage(httpProperties.getIndexPage()).setWebRoot(httpProperties.getWebRoot());

        // 权限认证处理器
        MyAuthProviderImpl authProvider = SpringContextHolder.getBean(MyAuthProviderImpl.class);
        SimpleAuthOptions authOptions = new SimpleAuthOptions();

        List<String> annos = new ArrayList<>();
        annos.add("POST:/login");
        if (httpProperties.isAuth()) {
            annos.addAll(httpProperties.getAnnoPermissions());
        } else {
            annos.add("*");
        }
        authOptions.setAnnoPermissions(annos);
        authOptions.setPermissionStrategy(new MyPermissionStrategyImpl());
        authOptions.setSessionIdStrategy(new MySessionIdStrategyImpl());
        SimpleAuthHandler authHandler = SimpleAuthHandler.create(this.vertx, authProvider, authOptions);

        router.route().handler(new CrosHandler());
        router.route().handler(CookieHandler.create());
        router.route().handler(authHandler);
        router.route().handler(BodyHandler.create());
        router.route().handler(new RequestParamsHandler());
        router.route().handler(new ResponsePreHandler());
        router.route().handler(staticHandler);
        // 挂载子路由
        List<SubRouter> subRouters = SpringContextHolder.getBeansListByType(SubRouter.class);
        subRouters.forEach(subRouter -> {
            subRouter.mount(router);
            log.info("http 子路由 " + subRouter.getClass().getSimpleName() + " 挂载成功");
        });

        // 此处可挂载各种日志处理器
        // ....

        // 挂载后置处理器
        router.route().handler(new ResponsePostHandler());            // 响应后置处理器, 对响应做最后的处理

    }
}
