package fun.bookish.vertx.auth.simple.configurable;

import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import io.vertx.core.http.Cookie;
import io.vertx.ext.web.Session;

public interface RememberMePersistStrategy {
    void cache(Cookie var1, Session var2);

    Session get(Cookie var1);

    void remove(Cookie var1);

    void clearExpired();

    void clearAll();

    RememberMePersistStrategy setOptions(SimpleAuthOptions var1);
}
