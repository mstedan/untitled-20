package fun.bookish.vertx.auth.simple.configurable.impl;

import fun.bookish.vertx.auth.simple.configurable.RealmStrategy;
import io.vertx.ext.web.RoutingContext;

public class DefaultRealmStrategyImpl implements RealmStrategy {
    public DefaultRealmStrategyImpl() {
    }

    public void afterAuthorisedSucceed(RoutingContext context) {
    }

    public void handleAuthorisedFailed(RoutingContext context) {
        context.response().setStatusCode(403).end("you have no permission to access '" + context.request().path() + "'");
    }

    public void handleAuthenticatedFailed(RoutingContext context) {
        context.response().setStatusCode(403).end("you need login first");
    }
}
