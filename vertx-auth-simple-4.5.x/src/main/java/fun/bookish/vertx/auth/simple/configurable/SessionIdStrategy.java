package fun.bookish.vertx.auth.simple.configurable;

import io.vertx.ext.web.RoutingContext;

public interface SessionIdStrategy {
    String getSessionId(RoutingContext var1);

    void writeSessionId(String var1, RoutingContext var2);
}
