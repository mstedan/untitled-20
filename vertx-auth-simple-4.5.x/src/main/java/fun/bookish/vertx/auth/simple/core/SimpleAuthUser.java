package fun.bookish.vertx.auth.simple.core;

import fun.bookish.vertx.auth.simple.configurable.PermissionStrategy;
import fun.bookish.vertx.auth.simple.configurable.impl.DefaultPermissionStrategyImpl;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authorization.Authorization;
import io.vertx.ext.auth.impl.UserImpl;
import lombok.Setter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SimpleAuthUser implements User {
    protected final Set<String> cachedPermissions = new HashSet();
    private final PermissionStrategy permissionStrategy;
    @Setter
    private volatile JsonObject principal;

    public SimpleAuthUser() {
        this.permissionStrategy = new DefaultPermissionStrategyImpl();
    }

    public SimpleAuthUser(PermissionStrategy permissionStrategy) {
        this.permissionStrategy = permissionStrategy;
    }

    protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        boolean access = this.cachedPermissions.stream().anyMatch((per) -> {
            return this.permissionStrategy.checkPermission(permission, per);
        });
        resultHandler.handle(Future.succeededFuture(access));
    }


    @Override
    public JsonObject attributes() {
        return null;
    }

    @Override
    public User isAuthorized(Authorization authorization, Handler<AsyncResult<Boolean>> handler) {
        return this;
    }

    @Override
    public User isAuthorized(String authority, Handler<AsyncResult<Boolean>> resultHandler) {
        if (this.cachedPermissions.contains(authority)) {
            resultHandler.handle(Future.succeededFuture(true));
        } else {
            this.doIsPermitted(authority, (res) -> {
                if (res.succeeded() && (Boolean)res.result()) {
                    this.cachedPermissions.add(authority);
                }

                resultHandler.handle(res);
            });
        }
        return this;
    }

    public JsonObject principal() {
        return this.principal;
    }

    public void setAuthProvider(AuthProvider authProvider) {
    }

    @Override
    public User merge(User user) {
        return null;
    }

    public void addPermission(String permission) {
        this.cachedPermissions.add(permission);
    }

    public void addPermissions(Collection<String> permissions) {
        this.cachedPermissions.addAll(permissions);
    }
}
