package com.example.verticle;


import com.example.auth.MyAuthProviderImpl;
import com.example.auth.MyPermissionStrategyImpl;
import com.example.auth.MySessionIdStrategyImpl;
import com.example.common.annotation.DeployVerticle;
import com.example.common.base.SubRouter;
import com.example.common.config.HttpProperties;
import com.example.common.handler.CrosHandler;
import com.example.common.handler.RequestParamsHandler;
import com.example.common.handler.ResponsePostHandler;
import com.example.common.handler.ResponsePreHandler;
import com.example.common.toolkit.spring.context.SpringContextHolder;
import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import fun.bookish.vertx.auth.simple.handler.SimpleAuthHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

/**
 * http服务部署单元
 */
@Slf4j
@DeployVerticle(2)
@Component
@Scope(SCOPE_PROTOTYPE)/*由于将部署此垂直领域的多个实例，因此需要原型范围*/
public class HttpVerticle extends AbstractVerticle {
    private final HttpProperties httpProperties = SpringContextHolder.getBean(HttpProperties.class);

    static final AtomicInteger counter = new AtomicInteger();
    @Override
    public void start() throws Exception {

        Router router = Router.router(vertx);

        //配置路由
        configRouter(router);

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(httpProperties.getPort(), res -> {
                    if (res.succeeded()) {
                        log.info("HttpServer-{} is runnig on localhost:{}", counter.incrementAndGet(), httpProperties.getPort());
                    } else {
                        log.error("http 服务启动失败", res.cause());
                        throw new IllegalStateException(res.cause());
                    }
                });

    }

    private void configRouter(Router router) {

        // 权限认证处理器
        MyAuthProviderImpl authProvider = SpringContextHolder.getBean(MyAuthProviderImpl.class);
        SimpleAuthOptions authOptions = new SimpleAuthOptions();

        List<String> annos = new ArrayList<>();
        if (httpProperties.isAuth()) {
            annos.addAll(httpProperties.getAnnoPermissions());
        } else {
            annos.add("*");
        }
        authOptions.setAnnoPermissions(annos);
        authOptions.setPermissionStrategy(new MyPermissionStrategyImpl());
        authOptions.setSessionIdStrategy(new MySessionIdStrategyImpl());
        SimpleAuthHandler authHandler = SimpleAuthHandler.create(this.vertx, authProvider, authOptions);

        // 挂载所有处理器
        router.route().handler(authHandler);                        // 权限认证处理器
        router.route().handler(BodyHandler.create());               // 请求体处理器
        router.route().handler(new CrosHandler());                  // 跨域处理器
        router.route().handler(new RequestParamsHandler());         // 请求参数处理器
        router.route().handler(new ResponsePreHandler());           // 响应前置处理器(设置字符编码等)
        router.route().handler(StaticHandler.create());             // 静态资源处理器
        // 挂载子路由
        List<SubRouter> subRouters = SpringContextHolder.getBeansListByType(SubRouter.class);
        subRouters.forEach(subRouter -> {
            subRouter.mount(router);
            if (log.isDebugEnabled()) {
                log.info("http 子路由 " + subRouter.getClass().getSimpleName() + " 挂载成功");
            }
        });

        // 此处可挂载各种日志处理器
        // ....

        // 挂载后置处理器
        router.route().handler(new ResponsePostHandler());            // 响应后置处理器, 对响应做最后的处理

    }
}
