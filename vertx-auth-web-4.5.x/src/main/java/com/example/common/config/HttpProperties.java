package com.example.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "http")
public class HttpProperties {

    private int port;                                               // 端口号
    private boolean auth;                                           // 是否开启权限校验
    private int instance = 1;                                       // verticle实例数
    private String indexPage = "index.html";                        // 首页
    private String webRoot = "dist";                                // 静态资源根目录
    private List<String> annoPermissions = new ArrayList<>();
}
