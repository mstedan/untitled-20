package com.example.router;

import com.example.common.base.SubRouter;
import com.example.common.toolkit.JsonUtil;
import com.example.common.toolkit.RoutingContextUtil;
import com.example.common.toolkit.VerifyCode;
import fun.bookish.vertx.auth.simple.core.Subject;
import fun.bookish.vertx.auth.simple.util.SubjectUtil;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component
public class LoginRouter implements SubRouter {

    @Override
    public void mount(Router router) {
        router.get("/hello").handler(this::hello);
        router.get("/logout").handler(this::logout);
        router.get("/verifyCode").handler(this::verifyCode);
        router.post("/login").handler(this::login);
    }

    private void verifyCode(RoutingContext context) {
        VerifyCode vc = new VerifyCode();
        BufferedImage image = vc.getImage();
        context.response().putHeader("content-type", "image/jpeg");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "JPEG", bos);
            byte[] byteArray = bos.toByteArray();
            Buffer buffer = Buffer.buffer();
            buffer.appendBytes(byteArray);
            context.response().end(buffer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void logout(RoutingContext context){
        Subject subject = SubjectUtil.getSubject(context);
        subject.logout(context);
        context.response().write(JsonUtil.getJsonStr(true,"注销成功"));
        context.next();
    }
    /**
     * 测试方法
     * @param context
     */
    private void hello(RoutingContext context) {
        context.response().write(JsonUtil.getJsonStr(true,"测试成功", "hello sv !"));
        context.next();
    }

    /**
     * 用户登录
     * @param context
     */
    private void login(RoutingContext context) {

        JsonObject params = RoutingContextUtil.getRequestParams(context);
        String username = params.getString("username");

        Subject subject = SubjectUtil.getSubject(context);
        subject.login(context, params, res -> {
            if (res.succeeded()) {
                context.response().write(JsonUtil.getJsonStr(true, "登录成功", username));
            } else {
                context.response().write(JsonUtil.getJsonStr(false, "用户名或密码错误", null));
            }
            context.next();
        });

    }

}
