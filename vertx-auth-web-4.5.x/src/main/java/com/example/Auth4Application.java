package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Auth4Application {
    public static void main(String[] args) {
        SpringApplication.run(Auth4Application.class, args);
    }
}
