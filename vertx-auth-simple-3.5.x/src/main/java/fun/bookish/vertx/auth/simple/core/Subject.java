package fun.bookish.vertx.auth.simple.core;

import fun.bookish.vertx.auth.simple.configurable.RememberMePersistStrategy;
import fun.bookish.vertx.auth.simple.configurable.SessionIdStrategy;
import fun.bookish.vertx.auth.simple.configurable.SessionPersistStrategy;
import fun.bookish.vertx.auth.simple.provider.SimpleAuthProvider;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class Subject {
    private String sessionId;
    private Vertx vertx;
    private SimpleAuthProvider authProvider;
    private SessionPersistStrategy sessionPersistStrategy;
    private SessionIdStrategy sessionIdStrategy;
    private RememberMePersistStrategy rememberMePersistStrategy;
    private Long rememberMeTimeout;
    private AtomicReference<User> authUserRef = new AtomicReference();
    private AtomicBoolean rememberMeRef = new AtomicBoolean(false);

    public Subject() {
    }

    public Subject(String sessionID, Vertx vertx, SimpleAuthProvider authProvider, SimpleAuthOptions options) {
        this.sessionId = sessionID;
        this.vertx = vertx;
        this.authProvider = authProvider;
        this.sessionPersistStrategy = options.getSessionPersistStrategy();
        this.sessionIdStrategy = options.getSessionIdStrategy();
        this.rememberMePersistStrategy = options.getRememberMePersistStrategy();
        this.rememberMeTimeout = options.getRememberMeTimeout();
    }

    public void login(RoutingContext ctx, JsonObject authInfo, Handler<AsyncResult<Void>> resultHandler) {
        this.vertx.executeBlocking((future) -> {
            this.authProvider.authenticate(authInfo, (res) -> {
                if (res.succeeded()) {
                    this.authUserRef.set(res.result());
                    ctx.session().put("SIMPLE-AUTH-SUBJECT", this);

                    try {
                        this.sessionPersistStrategy.cache(ctx.session());
                        if (this.rememberMeRef.get()) {
                            Cookie cookie = Cookie.cookie("RememberMe", ctx.session().id()).setMaxAge(this.rememberMeTimeout).setHttpOnly(false).setPath("/");
                            this.rememberMePersistStrategy.cache(cookie, ctx.session());
                            ctx.addCookie(cookie);
                        }
                    } catch (Exception var5) {
                        future.fail(var5);
                    }

                    future.complete();
                } else {
                    future.fail(res.cause());
                }

            });
        }, resultHandler);
    }

    public void isAuthorised(String authority, Handler<AsyncResult<Boolean>> resultHandler) {
        if (this.authUserRef.get() == null) {
            resultHandler.handle(Future.succeededFuture(false));
        } else {
            ((User)this.authUserRef.get()).isAuthorised(authority, resultHandler);
        }

    }

    public void logout(RoutingContext ctx) {
        this.authUserRef.set((User) null);
        ctx.setUser((User)null);
        this.sessionPersistStrategy.remove(ctx.session());
        Cookie rememberMeCookie = ctx.removeCookie("RememberMe");
        if (rememberMeCookie != null) {
            this.rememberMePersistStrategy.remove(rememberMeCookie);
        }

    }

    public boolean isAuthenticated() {
        return this.authUserRef.get() != null;
    }

    public User getUser() {
        return (User)this.authUserRef.get();
    }

    public JsonObject getPrincipal() {
        return this.authUserRef.get() == null ? null : ((User)this.authUserRef.get()).principal();
    }

    public Subject enableRememberMe(boolean state) {
        this.rememberMeRef.set(state);
        return this;
    }
}
