package fun.bookish.vertx.auth.simple.core;

import fun.bookish.vertx.auth.simple.configurable.PermissionStrategy;
import fun.bookish.vertx.auth.simple.configurable.impl.DefaultPermissionStrategyImpl;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;
import lombok.Setter;

import java.util.Collection;

public class SimpleAuthUser extends AbstractUser {
    private final PermissionStrategy permissionStrategy;
    @Setter
    private volatile JsonObject principal;

    public SimpleAuthUser() {
        this.permissionStrategy = new DefaultPermissionStrategyImpl();
    }

    public SimpleAuthUser(PermissionStrategy permissionStrategy) {
        this.permissionStrategy = permissionStrategy;
    }

    protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        boolean access = this.cachedPermissions.stream().anyMatch((per) -> {
            return this.permissionStrategy.checkPermission(permission, per);
        });
        resultHandler.handle(Future.succeededFuture(access));
    }


    public JsonObject principal() {
        return this.principal;
    }

    public void setAuthProvider(AuthProvider authProvider) {
    }

    public void addPermission(String permission) {
        this.cachedPermissions.add(permission);
    }

    public void addPermissions(Collection<String> permissions) {
        this.cachedPermissions.addAll(permissions);
    }
}
