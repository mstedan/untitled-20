package fun.bookish.vertx.auth.simple.configurable.impl;

import fun.bookish.vertx.auth.simple.configurable.RememberMePersistStrategy;
import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Session;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class DefaultRememberMePersistStrategyImpl implements RememberMePersistStrategy {
    private SimpleAuthOptions options;
    private static final ConcurrentHashMap<String, Session> SESSION_CACHE = new ConcurrentHashMap();
    private AtomicBoolean clearExpiredTaskStarted = new AtomicBoolean(false);

    public DefaultRememberMePersistStrategyImpl() {
    }

    public void cache(Cookie rememberMeCookie, Session session) {
        this.checkClearExpiredTask();
        SESSION_CACHE.put(rememberMeCookie.getValue(), session);
    }

    public Session get(Cookie rememberMeCookie) {
        this.checkClearExpiredTask();
        return (Session)SESSION_CACHE.get(rememberMeCookie.getValue());
    }

    public void remove(Cookie rememberMeCookie) {
        this.checkClearExpiredTask();
        SESSION_CACHE.remove(rememberMeCookie.getValue());
    }

    public void clearExpired() {
        Long cookieTimeout = this.options.getSessionTimeout();
        this.clearExpiredSession(cookieTimeout);
    }

    public void clearAll() {
        this.checkClearExpiredTask();
        SESSION_CACHE.clear();
    }

    public RememberMePersistStrategy setOptions(SimpleAuthOptions options) {
        this.options = options;
        return this;
    }

    private void checkClearExpiredTask() {
        if (!this.clearExpiredTaskStarted.get()) {
            Long cookieTimeout = this.options.getRememberMeTimeout();
            this.options.getVertx().setPeriodic(cookieTimeout * 1000L, (id) -> {
                this.clearExpiredSession(cookieTimeout);
            });
        }

    }

    private void clearExpiredSession(Long cookieTimeout) {
        LocalDateTime now = LocalDateTime.now();
        Set<Map.Entry<String, Session>> entrySet = SESSION_CACHE.entrySet();
        Iterator var4 = entrySet.iterator();

        while(var4.hasNext()) {
            Map.Entry<String, Session> entry = (Map.Entry)var4.next();
            LocalDateTime sessionCreate = (LocalDateTime)((Session)entry.getValue()).get("SESSION-CREATE-TIME");
            if (sessionCreate.plusSeconds(cookieTimeout).isBefore(now)) {
                SESSION_CACHE.remove(entry.getKey());
            }
        }
    }
}
