package fun.bookish.vertx.auth.simple.configurable;

import io.vertx.ext.web.RoutingContext;

public interface RealmStrategy {
    void afterAuthorisedSucceed(RoutingContext var1);

    void handleAuthorisedFailed(RoutingContext var1);

    void handleAuthenticatedFailed(RoutingContext var1);
}
