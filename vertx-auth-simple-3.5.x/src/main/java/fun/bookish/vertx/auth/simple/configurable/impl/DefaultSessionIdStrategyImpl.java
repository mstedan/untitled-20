package fun.bookish.vertx.auth.simple.configurable.impl;

import fun.bookish.vertx.auth.simple.configurable.SessionIdStrategy;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;

public class DefaultSessionIdStrategyImpl implements SessionIdStrategy {
    public DefaultSessionIdStrategyImpl() {
    }

    public String getSessionId(RoutingContext context) {
        return context.getCookie("JSESSIONID") == null ? null : context.getCookie("JSESSIONID").getValue();
    }

    public void writeSessionId(String sessionId, RoutingContext context) {
        Cookie cookie = Cookie.cookie("JSESSIONID", sessionId).setHttpOnly(true).setPath("/");
        context.addCookie(cookie);
    }
}
