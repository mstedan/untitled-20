package fun.bookish.vertx.auth.simple.core;

import fun.bookish.vertx.auth.simple.configurable.*;
import fun.bookish.vertx.auth.simple.configurable.impl.DefaultRememberMePersistStrategyImpl;
import fun.bookish.vertx.auth.simple.configurable.impl.DefaultSessionPersistStrategyImpl;
import fun.bookish.vertx.auth.simple.constant.SimpleAuthConstants;
import io.vertx.core.Vertx;

import java.util.List;

public class SimpleAuthOptions {
    private Vertx vertx;
    private PermissionStrategy permissionStrategy;
    private SessionIdStrategy sessionIdStrategy;
    private SessionPersistStrategy sessionPersistStrategy;
    private RememberMePersistStrategy rememberMePersistStrategy;
    private RealmStrategy realmStrategy;
    private Long sessionTimeout;
    private Long rememberMeTimeout;
    private List<String> annoPermissions;

    public SimpleAuthOptions() {
        this.permissionStrategy = SimpleAuthConstants.DEFAULT_PERMISSION_STRATEGY_IMPL;
        this.sessionIdStrategy = SimpleAuthConstants.DEFAULT_SESSION_ID_STRATEGY_IMPL;
        this.sessionPersistStrategy = (new DefaultSessionPersistStrategyImpl()).setOptions(this);
        this.rememberMePersistStrategy = (new DefaultRememberMePersistStrategyImpl()).setOptions(this);
        this.realmStrategy = SimpleAuthConstants.DEFAULT_REALM_STRATEGY_IMPL;
        this.sessionTimeout = 1200000L;
        this.rememberMeTimeout = 1296000L;
    }

    public PermissionStrategy getPermissionStrategy() {
        return this.permissionStrategy;
    }

    public SimpleAuthOptions setPermissionStrategy(PermissionStrategy permissionStrategy) {
        if (permissionStrategy == null) {
            throw new IllegalArgumentException("permissionStrategy can not be null");
        } else {
            this.permissionStrategy = permissionStrategy;
            return this;
        }
    }

    public SessionIdStrategy getSessionIdStrategy() {
        return this.sessionIdStrategy;
    }

    public SimpleAuthOptions setSessionIdStrategy(SessionIdStrategy sessionIdStrategy) {
        if (sessionIdStrategy == null) {
            throw new IllegalArgumentException("sessionIdStrategy can not be null");
        } else {
            this.sessionIdStrategy = sessionIdStrategy;
            return this;
        }
    }

    public SessionPersistStrategy getSessionPersistStrategy() {
        return this.sessionPersistStrategy;
    }

    public SimpleAuthOptions setSessionPersistStrategy(SessionPersistStrategy sessionPersistStrategy) {
        if (sessionPersistStrategy == null) {
            throw new IllegalArgumentException("sessionPersistStrategy can not be null");
        } else {
            this.sessionPersistStrategy = sessionPersistStrategy;
            return this;
        }
    }

    public RealmStrategy getRealmStrategy() {
        return this.realmStrategy;
    }

    public SimpleAuthOptions setRealmStrategy(RealmStrategy realmStrategy) {
        if (realmStrategy == null) {
            throw new IllegalArgumentException("realmStrategy can not be null");
        } else {
            this.realmStrategy = realmStrategy;
            return this;
        }
    }

    public List<String> getAnnoPermissions() {
        return this.annoPermissions;
    }

    public SimpleAuthOptions setAnnoPermissions(List<String> annoPermissions) {
        this.annoPermissions = annoPermissions;
        return this;
    }

    public Long getSessionTimeout() {
        return this.sessionTimeout;
    }

    public SimpleAuthOptions setSessionTimeout(Long sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
        return this;
    }

    public Vertx getVertx() {
        return this.vertx;
    }

    public SimpleAuthOptions setVertx(Vertx vertx) {
        this.vertx = vertx;
        return this;
    }

    public Long getRememberMeTimeout() {
        return this.rememberMeTimeout;
    }

    public SimpleAuthOptions setRememberMeTimeout(Long rememberMeTimeout) {
        this.rememberMeTimeout = rememberMeTimeout;
        return this;
    }

    public RememberMePersistStrategy getRememberMePersistStrategy() {
        return this.rememberMePersistStrategy;
    }

    public SimpleAuthOptions setRememberMePersistStrategy(RememberMePersistStrategy rememberMePersistStrategy) {
        if (rememberMePersistStrategy == null) {
            throw new IllegalArgumentException("rememberMePersistStrategy can not be null");
        } else {
            this.rememberMePersistStrategy = rememberMePersistStrategy;
            return this;
        }
    }
}
