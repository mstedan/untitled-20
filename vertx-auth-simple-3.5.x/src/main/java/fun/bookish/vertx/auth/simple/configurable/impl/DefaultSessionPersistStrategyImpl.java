package fun.bookish.vertx.auth.simple.configurable.impl;

import fun.bookish.vertx.auth.simple.configurable.SessionPersistStrategy;
import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import io.vertx.ext.web.Session;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class DefaultSessionPersistStrategyImpl implements SessionPersistStrategy {

    private SimpleAuthOptions options;
    private static final ConcurrentHashMap<String, Session> SESSION_CACHE = new ConcurrentHashMap();
    private static final AtomicBoolean CLEAR_EXPIRED_TASK_STARTED = new AtomicBoolean(false);

    public DefaultSessionPersistStrategyImpl() {
    }

    public void cache(Session session) {
        this.checkClearExpiredTask();
        SESSION_CACHE.put(session.id(), session);
    }

    public Session get(String sessionId) {
        this.checkClearExpiredTask();
        return sessionId == null ? null : (Session)SESSION_CACHE.get(sessionId);
    }

    public void remove(Session session) {
        this.checkClearExpiredTask();
        SESSION_CACHE.remove(session.id());
    }

    public void clearExpired() {
        Long sessionTimeout = this.options.getSessionTimeout();
        this.clearExpiredSession(sessionTimeout);
    }

    public void clearAll() {
        this.checkClearExpiredTask();
        SESSION_CACHE.clear();
    }

    public SessionPersistStrategy setOptions(SimpleAuthOptions options) {
        this.options = options;
        return this;
    }

    private void checkClearExpiredTask() {
        if (!CLEAR_EXPIRED_TASK_STARTED.get()) {
            Long sessionTimeout = this.options.getSessionTimeout();
            this.options.getVertx().setPeriodic(sessionTimeout * 1000L, (id) -> {
                this.clearExpiredSession(sessionTimeout);
            });
        }

    }

    private void clearExpiredSession(Long sessionTimeOut) {
        LocalDateTime now = LocalDateTime.now();
        Set<Map.Entry<String, Session>> entrySet = SESSION_CACHE.entrySet();
        Iterator var4 = entrySet.iterator();

        while(var4.hasNext()) {
            Map.Entry<String, Session> entry = (Map.Entry)var4.next();
            LocalDateTime sessionCreate = (LocalDateTime)((Session)entry.getValue()).get("SESSION-CREATE-TIME");
            if (sessionCreate.plusSeconds(sessionTimeOut).isBefore(now)) {
                SESSION_CACHE.remove(entry.getKey());
            }
        }

    }
}
