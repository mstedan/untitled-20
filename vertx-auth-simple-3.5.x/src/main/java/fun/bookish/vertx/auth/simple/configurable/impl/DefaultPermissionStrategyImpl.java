package fun.bookish.vertx.auth.simple.configurable.impl;

import fun.bookish.vertx.auth.simple.configurable.PermissionStrategy;
import io.vertx.core.http.HttpServerRequest;

public class DefaultPermissionStrategyImpl implements PermissionStrategy {
    public DefaultPermissionStrategyImpl() {
    }

    public String generatePermission(HttpServerRequest request) {
        return request.path();
    }

    public boolean checkPermission(String request, String cached) {
        return cached.equals("*") || request.equals(cached) || request.contains(".html") || request.contains(".css") || request.contains(".json") || request.contains(".text") || request.contains(".js") || request.contains(".woff") || request.contains(".svg") || request.contains(".ttf") || request.contains(".ico") || request.contains(".png");
    }
}
