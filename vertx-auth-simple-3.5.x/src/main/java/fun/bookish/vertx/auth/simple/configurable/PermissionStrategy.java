package fun.bookish.vertx.auth.simple.configurable;

import io.vertx.core.http.HttpServerRequest;

public interface PermissionStrategy {
    String generatePermission(HttpServerRequest var1);

    boolean checkPermission(String var1, String var2);
}
