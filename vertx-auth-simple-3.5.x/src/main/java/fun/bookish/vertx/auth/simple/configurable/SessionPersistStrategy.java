package fun.bookish.vertx.auth.simple.configurable;

import fun.bookish.vertx.auth.simple.core.SimpleAuthOptions;
import io.vertx.ext.web.Session;

public interface SessionPersistStrategy {
    void cache(Session var1);

    Session get(String var1);

    void remove(Session var1);

    void clearExpired();

    void clearAll();

    SessionPersistStrategy setOptions(SimpleAuthOptions var1);
}
