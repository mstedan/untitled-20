package fun.bookish.vertx.auth.simple.util;

import fun.bookish.vertx.auth.simple.core.Subject;
import io.vertx.ext.web.RoutingContext;

public class SubjectUtil {
    private SubjectUtil() {
    }

    public static Subject getSubject(RoutingContext context) {
        return (Subject)context.session().get("SIMPLE-AUTH-SUBJECT");
    }
}
